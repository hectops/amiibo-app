import 'package:amiibo/model/amiibo.dart';
import 'package:amiibo/redux/state/amiibo_app_state.dart';
import 'package:amiibo/ui/screens/amiibo_detail_screen.dart';
import 'package:amiibo/ui/screens/amiibo_grid_screen.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import "package:flare_flutter/flare_actor.dart";

class Utils {
  static Widget generateHorizontalCardListView(List<Amiibo> amiiboList, BuildContext context) {
    
    List<Widget> amiibosListView = generateAmiiboWidgetList(amiiboList, context);

    if(amiiboList.length > 10) {
      amiibosListView = amiibosListView.take(10).toList();
      amiibosListView.add(
        Container(
          height: 200,
          width: 200,
          child: Card(
            elevation: 2,
            child: FlatButton(
              child: Padding(
                padding: EdgeInsets.all(5),
                child: Text(
                  "More",
                  style: TextStyle(
                    fontSize: 32,
                    fontWeight: FontWeight.w200
                  ),
                ),
              ),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(
                  builder: (BuildContext context) => AmiiboGridScreen(amiiboList: amiiboList,))
                );
              },
            ),
          ),
        )
      );
    }

    return Container(
      height: 200,
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.only(
          top: 5,
        ),
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: amiibosListView
        ),
      ),
    );
  }
  
  
  /// Generate a Map<String, List<Amiibo>> based on the provided 
  /// order criteria. The key is the title on top of the lists 
  /// in the home screen. 

  static Map<String, List<Amiibo>> groupAmiibosByCriteria(List<Amiibo> amiiboList, OrderCriteria orderCriteria) {
    Map<String, List<Amiibo>> gruopedMap = {};

    amiiboList.forEach((amiibo) {

      switch (orderCriteria) {
        case OrderCriteria.amiiboSeries:
          if(gruopedMap[amiibo.amiiboSeries] == null) {
            gruopedMap[amiibo.amiiboSeries] = [];
          }
          gruopedMap[amiibo.amiiboSeries].add(amiibo);
          break;
        case OrderCriteria.character:
          if(gruopedMap[amiibo.character] == null) {
            gruopedMap[amiibo.character] = [];
          }
          gruopedMap[amiibo.character].add(amiibo);
          break;
        case OrderCriteria.gameSeries:
          if(gruopedMap[amiibo.gameSeries] == null) {
            gruopedMap[amiibo.gameSeries] = [];
          }
          gruopedMap[amiibo.gameSeries].add(amiibo);
          break;
        case OrderCriteria.type:
          if(gruopedMap[amiibo.type] == null) {
            gruopedMap[amiibo.type] = [];
          }
          gruopedMap[amiibo.type].add(amiibo);
          break;
        default:
          if(gruopedMap[amiibo.amiiboSeries] == null) {
            gruopedMap[amiibo.amiiboSeries] = [];
          }
          gruopedMap[amiibo.amiiboSeries].add(amiibo);
      }
    });
    
    return gruopedMap;
  }

  static List<Widget> generateAmiiboWidgetList(List<Amiibo> amiiboList, BuildContext context) {
    
    //Map each amiibo to a card
    return amiiboList.map<Widget>((Amiibo amiibo) {
      return Container(
        height: 200,
        width: 200,
        child: Card(
          elevation: 2,
          child: FlatButton(
            child: Padding(
              padding: EdgeInsets.all(5),
              child: Hero(
                tag: amiibo.hashCode,
                child: CachedNetworkImage(
                  placeholder: FlareActor(
                    "resources/amiibo.flr",
                    fit: BoxFit.contain,
                    animation: "spinning",
                  ), 
                  imageUrl: amiibo.image
                ),
              ),
            ),
            onPressed: () {
              print(amiibo.name);
              Navigator.pushAndRemoveUntil(context, 
                  MaterialPageRoute(builder: (context) => AmiiboDetailScreen(amiibo: amiibo,)), 
                  (route) {return route.isFirst;});
            },
          ),
        ),
      );
    }).toList();
  }

  static List<Amiibo> filterList(List<Amiibo> amiiboList, String searchText) {
    if (searchText.isEmpty)
      return amiiboList;
    final RegExp regexp = RegExp(
      searchText.replaceAll(RegExp(r'[!@#$%^&|*()_+={}\[\]:;"\-<,>.?\/\\]'), ""), 
      caseSensitive: false,);
    return amiiboList.where((Amiibo amiibo) => amiibo.name.contains(regexp)).toList();
  }

  static List<Amiibo> strictFilterList(List<Amiibo> amiiboList, Amiibo amiibo) {
    return amiiboList
        .where((inner) => inner.hashCode != amiibo.hashCode && inner.character.compareTo(amiibo.character) == 0)
        .toList();
  }
}