import 'package:quiver/core.dart';

class Amiibo {  

  String amiiboSeries;
  String character;
  String gameSeries;
  String head;
  String image;
  String name;
  Release release;
  String tail;
  String type; 

  Amiibo({ 
    this.amiiboSeries,this.character,this.gameSeries,this.head,this.image,this.name,this.release,this.tail,this.type,
  });

  Amiibo copyWith({amiiboSeries, character, gameSeries, head,
      image, name, release, tail, type,}) {
    return Amiibo(
      amiiboSeries: amiiboSeries ?? this.amiiboSeries,
      character: character ?? this.character,
      gameSeries: gameSeries ?? this.gameSeries,
      head: head ?? this.head,
      image: image ?? this.image,
      name: name ?? this.name,
      release: release ?? this.release,
      tail: tail ?? this.tail,
      type: type ?? this.type,  
    );
  }
  
  static Amiibo fromJson(Map<String,dynamic> json){
    return Amiibo( 
      amiiboSeries: json['amiiboSeries'],
      character: json['character'],
      gameSeries: json['gameSeries'],
      head: json['head'],
      image: json['image'],
      name: json['name'],
      release: Release.fromJson(json['release']),
      tail: json['tail'],
      type: json['type'],
    );
  }
  
  Map<String, dynamic> toJson() => { 
    'amiiboSeries': amiiboSeries,
    'character': character,
    'gameSeries': gameSeries,
    'head': head,
    'image': image,
    'name': name,
    'release': release.toJson(),
    'tail': tail,
    'type': type,
  };

  @override
  String toString() {
    return toJson().toString();
  }

  @override
  int get hashCode => hash2(name.hashCode, image.hashCode);
}

class Release {
  String au;
  String eu;
  String jp;
  String na;

  Release({this.au, this.eu, this.jp, this.na});

  Release copyWith({au, eu, jp, na}) {
    return Release(
      au: au ?? this.au,
      eu: eu ?? this.eu,
      jp: jp ?? this.jp,
      na: na ?? this.na,
    );
  }

  static Release fromJson(Map<String,dynamic> json){
    return Release( 
      au: json['au'],
      eu: json['eu'],
      jp: json['jp'],
      na: json['na'],
    );
  }

  Map<String, dynamic> toJson() => { 
    'au': au,
    'eu': eu,
    'jp': jp,
    'na': na,
  };

  @override
  String toString() {
    return toJson().toString();
  }

  String toDisplayString() {
    String response = "";

    response += au != null ? "Australia: " + au + "\n" : "";
    response += eu != null ? "Europe: " + eu + "\n" : "";
    response += jp != null ? "Japan: " + jp + "\n" : "";
    response += na != null ? "North America: " + na : "";

    return response;
  }
}