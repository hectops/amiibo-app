import 'package:amiibo/model/amiibo.dart';
import 'package:amiibo/redux/actions/get_amiibos_list_action.dart';
import 'package:amiibo/redux/actions/set_amiibo_loading_state_action.dart';
import 'package:amiibo/redux/actions/set_order_criteria_action.dart';
import 'package:amiibo/redux/actions/toggle_interface_mode_action.dart';
import 'package:amiibo/redux/amiibo_app_state_store.dart';
import 'package:amiibo/redux/state/amiibo_app_state.dart';
import 'package:amiibo/util/utils.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';

import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class AmiiboHomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AmiiboHomeScreenState();
  }
}

class _AmiiboHomeScreenState extends State<AmiiboHomeScreen> {

  bool _isSearching = false;
  String _searchText = "";
  DecoratedBox _bottomSheet;
  
  @override
  Widget build(BuildContext context) {
    
    return StoreBuilder<AmiiboAppState>(
      onInit: (amiiboAppStore) => amiiboAppStore.dispatch(GetAmiibosListAction()),
      builder: (BuildContext context, Store<AmiiboAppState> amiiboAppStore) {
  
        return Scaffold(
          appBar: _buildAppBar(context, amiiboAppStore),
          body: SafeArea(
            child: _buildBody(amiiboAppStore),
          ),
          bottomSheet: _bottomSheet,
        );
      }
    );
  }

  Widget _buildBody(Store<AmiiboAppState> amiiboAppStore) {
    switch (amiiboAppStore.state.amiiboLoadingState) {
      case LoadingState.didLoad:
        return ListView(
          children: Utils.groupAmiibosByCriteria(amiiboAppStore.state.amiibosList, amiiboAppStore.state.orderCriteria)
              .map<String, Widget>((categoryName, amiiboList) {

            List<Amiibo> filteredList = Utils.filterList(amiiboList, _searchText);

            if(filteredList.isNotEmpty) {
              return MapEntry(categoryName,
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(7),
                    child: Text(
                      categoryName,
                      style: TextStyle(
                        fontSize: 32,
                        fontWeight: FontWeight.w200
                      ),
                    ),
                  ),
                  Utils.generateHorizontalCardListView(filteredList, context,),
                ],
              ));
            } 
            else {
              return MapEntry(categoryName, SizedBox());
            }
          }).values.toList()
        );
        break;
      case LoadingState.isLoading:
        return Center(
          child:  Container(
            height: 200,
            width: 200,
            child :FlareActor(
              "resources/amiibo.flr",
              fit: BoxFit.contain,
              animation: "pulse",
            ),
          ),
        );
      default:
        return Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Trouble Loading Data",
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w300,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              RaisedButton(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Text(
                    "Retry",
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                ),
                color: Theme.of(context).buttonTheme.colorScheme.surface,
                onPressed: () {
                  amiiboAppStore.dispatch(SetAmiiboLoadingStateAction(amiiboLoadingState: LoadingState.isLoading));
                  amiiboAppStore.dispatch(GetAmiibosListAction());
                },
              ),
            ],
          )
        );
    }
  }

  AppBar _buildAppBar(BuildContext context, Store<AmiiboAppState> amiiboAppStore) {
    if(_isSearching) {
      return AppBar(
        leading: BackButton(
          color: Theme.of(context).accentColor,
        ),
        title: TextField(
          autofocus: true,
          decoration: const InputDecoration(
            hintText: 'Search Amiibos',
          ),
          onChanged: (text) {
            print(text);
            setState(() {_searchText = text;});
          },
        ),
        backgroundColor: Theme.of(context).canvasColor,
      );
    } 
    else {
      return AppBar(
        title: Text("Amiibos"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search), 
            onPressed: () {
              print("Search Pressed");
              ModalRoute.of(context).addLocalHistoryEntry(
                LocalHistoryEntry(
                  onRemove: () {
                    setState(() {
                      _isSearching = !_isSearching;
                      _searchText = "";
                    });
                  }
                )
              );
              setState(() {_isSearching = !_isSearching;});
            }
          ),
          IconButton(
            icon: Icon(Icons.filter_list),
            onPressed: (){
              setState(() {
                _bottomSheet = _bottomSheet == null ? 
                    _getConfigurationSheet(amiiboAppStore.state.orderCriteria) : null;
              });
            },
          ),
          PopupMenuButton(
            onSelected: (index) {
              switch (index) {
                case 0:
                  amiiboAppStore.dispatch(ToggleInterfaceMode());
                  print("Pressed Interface Mode");
                  break;
                case 1:
                  Navigator.pushNamed(context, "/about");
                  print("Pressed About");
                  break;
                default:
              }
            },
            itemBuilder: (BuildContext context) {
              return <PopupMenuEntry> [
                PopupMenuItem(
                  value: 0,
                  child: Text("Interface Mode"),
                ),
                PopupMenuItem(
                  value: 1,
                  child: Text("About"),
                ),
              ];
            },
          )
        ],
      );
    } 
  }

  DecoratedBox _getConfigurationSheet(OrderCriteria selectedCriteria) {
    return  DecoratedBox(
      decoration: const BoxDecoration(
        border: Border(top: BorderSide(color: Colors.black26)),
      ),
      child: ListView(
        shrinkWrap: true,
        primary: false,
        children: <Widget>[
          RadioListTile<OrderCriteria>(
            dense: true,
            title: const Text('Order by Amiibo Series'),
            value: OrderCriteria.amiiboSeries,
            groupValue: selectedCriteria,
            onChanged: changeOrderCriteria,
          ),
          RadioListTile<OrderCriteria>(
            dense: true,
            title: const Text('Order by Game Series'),
            value: OrderCriteria.gameSeries,
            groupValue: selectedCriteria,
            onChanged: changeOrderCriteria,
          ),
          RadioListTile<OrderCriteria>(
            dense: true,
            title: const Text('Order by Character'),
            value: OrderCriteria.character,
            groupValue: selectedCriteria,
            onChanged: changeOrderCriteria,
          ),
          RadioListTile<OrderCriteria>(
            dense: true,
            title: const Text('Order by Amiibo Type'),
            value: OrderCriteria.type,
            groupValue: selectedCriteria,
            onChanged: changeOrderCriteria,
          ),
        ],
      ),
    );
  }

  changeOrderCriteria(OrderCriteria orderCriteria) {
    amiiboAppStore.dispatch(SetOrderCriteriaAction(orderCriteria: orderCriteria));
    setState(() {
      _bottomSheet = null;
    });
  }
}