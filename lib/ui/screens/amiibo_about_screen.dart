

import 'package:amiibo/redux/state/amiibo_app_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:url_launcher/url_launcher.dart';

class AmiiboAboutScreen extends StatelessWidget {

  Widget build(BuildContext context) {
    
    return StoreConnector<AmiiboAppState, AmiiboAppState>(
      converter: (Store<AmiiboAppState> amiiboAppStore) => amiiboAppStore.state,
      builder: (BuildContext context, AmiiboAppState amiibosAppState) {
  
        return Scaffold(
          appBar: AppBar(
            title: Text("About"),
          ),
          body: SafeArea(
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      "Disclaimer",
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Amiibo is a Nintendo registered trade mark. This app has no affiliation with"
                      + " Nintendo or any other companies that own the rights to it. The purpose of this app is" 
                      + " not to show a comprehensive list of all amiibos and the list may not be"
                      + " up to date. The user agrees to the terms of the API stated below and that the"
                      + " use of this app is entirely at user’s own risks. We are not responsible for any"
                      + " misunderstandings and always recomend official sources be used.",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w300
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Why?",
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      "This app was made to learn flutter, flare and redux.",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w300
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      "API",
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    InkWell(
                      child: Text(
                        "http://www.amiiboapi.com",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                          decoration: TextDecoration.underline,
                        ),
                      ),
                      onTap: () => _launchURL("http://www.amiiboapi.com"),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      "The Code",
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    InkWell(
                      child: Text(
                        "https://gitlab.com/hectops/amiibo-app",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                          decoration: TextDecoration.underline
                        ),
                      ),
                      onTap: () => _launchURL("https://gitlab.com/hectops/amiibo-app"),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      }
    );
  }

  void _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}