
import 'package:amiibo/model/amiibo.dart';
import 'package:amiibo/redux/state/amiibo_app_state.dart';
import 'package:amiibo/util/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class AmiiboGridScreen extends StatelessWidget {

  final List<Amiibo> amiiboList;

  const AmiiboGridScreen({Key key, @required this.amiiboList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    return StoreConnector<AmiiboAppState, OrderCriteria>(
      converter: (Store<AmiiboAppState> amiiboAppStore) => amiiboAppStore.state.orderCriteria,
      builder: (BuildContext context, OrderCriteria orderCriteria) {
        return Scaffold(
          appBar: AppBar(
            title: _getTitle(orderCriteria),
          ),
          body: SafeArea(
            child: GridView.count(
              crossAxisCount: 2,
              children: Utils.generateAmiiboWidgetList(amiiboList, context),
            )
          ),
        );
      }
    );
  }

  Text _getTitle(OrderCriteria orderCriteria) {
    Text appBarTitle;

    switch (orderCriteria) {
      case OrderCriteria.amiiboSeries:
        appBarTitle =Text(amiiboList[0].amiiboSeries);
        break;
      case OrderCriteria.character:
        appBarTitle =Text(amiiboList[0].character);
        break;
      case OrderCriteria.gameSeries:
        appBarTitle =Text(amiiboList[0].gameSeries);
        break;
      case OrderCriteria.type:
        appBarTitle =Text(amiiboList[0].type);
        break;
    }

    return appBarTitle;
  }
}