import 'package:amiibo/model/amiibo.dart';
import 'package:amiibo/redux/state/amiibo_app_state.dart';
import 'package:amiibo/util/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'package:redux/redux.dart';

class AmiiboDetailScreen extends StatelessWidget {

  final Amiibo amiibo;

  AmiiboDetailScreen({Key key, @required this.amiibo}) : super(key: key);

  Widget build(BuildContext context) {

    return StoreConnector<AmiiboAppState, List<Amiibo>>(
      converter: (Store<AmiiboAppState> amiiboAppStore) => amiiboAppStore.state.amiibosList,
      builder: (BuildContext context, List amiiboList) {
        
        return Scaffold(
          appBar: AppBar(
            title: Text(amiibo.name),
          ),
          bottomSheet: null,
          body: SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 30,
                  ),
                  Container(
                    height: 250 ,
                    child: Hero(
                      tag: amiibo.hashCode,
                      child: CachedNetworkImage(
                        placeholder: FlareActor(
                          "resources/amiibo.flr",
                          fit: BoxFit.contain,
                          animation: "spinning",
                        ), 
                        imageUrl: amiibo.image
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Card(
                    child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Amiibo Series",
                            style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.w500
                            ),
                          ),
                          Text(
                            amiibo.amiiboSeries,
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w300
                            ),
                          ),
                          Divider(
                            color: Theme.of(context).accentColor,
                            height: 25.0,
                          ),
                          Text(
                            "Game Series",
                            style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.w500
                            ),
                          ),
                          Text(
                            amiibo.gameSeries,
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w300
                            ),
                          ),
                          Divider(
                            color: Theme.of(context).accentColor,
                            height: 25.0,
                          ),
                          Text(
                            "Amiibo Type",
                            style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.w500
                            ),
                          ),
                          Text(
                            amiibo.type,
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w300
                            ),
                          ),
                          Divider(
                            color: Theme.of(context).accentColor,
                            height: 25.0,
                          ),
                          Text(
                            "Releases",
                            style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.w500
                            ),
                          ),
                          Text(
                            amiibo.release.toDisplayString(),
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w300
                            ),
                          ),
                          _getSameCharacterAmiiboTitle(context, amiiboList),
                        ],
                      ),
                    )
                  ),
                  _generateSameAmiiboList(context, amiiboList),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _getSameCharacterAmiiboTitle(BuildContext context, List<Amiibo> amiiboList) {
    if(Utils.strictFilterList(amiiboList, amiibo).isNotEmpty) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Divider(
            color: Theme.of(context).accentColor,
            height: 25.0,
          ),
          Text(
            "Other " + amiibo.character + " Amiibos" ,
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.w500
            ),
          ),
        ],
      );
    }
    else {
      return SizedBox();
    }
  }

  Widget _generateSameAmiiboList(BuildContext context, List<Amiibo> amiiboList) {
    if(Utils.strictFilterList(amiiboList, amiibo).isNotEmpty) {
      return Utils.generateHorizontalCardListView(Utils.strictFilterList(amiiboList, amiibo), context);
    }
    else {
      return SizedBox();
    }
  }
}