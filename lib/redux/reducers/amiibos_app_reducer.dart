
import 'package:amiibo/redux/actions/set_amiibo_loading_state_action.dart';
import 'package:amiibo/redux/actions/set_amiibos_list_action.dart';
import 'package:amiibo/redux/actions/set_interface_mode_action.dart';
import 'package:amiibo/redux/actions/set_order_criteria_action.dart';
import 'package:amiibo/redux/actions/toggle_interface_mode_action.dart';
import 'package:amiibo/redux/state/amiibo_app_state.dart';
import 'package:redux/redux.dart';

Reducer<AmiiboAppState> amiiboAppReducer = combineReducers([
  TypedReducer<AmiiboAppState, SetAmiibosListAction>(setAmiibosListReducer),
  TypedReducer<AmiiboAppState, ToggleInterfaceMode>(toggleInterfaceModeReducer),
  TypedReducer<AmiiboAppState, SetInterfaceModeAction>(setInterfaceModeReducer),
  TypedReducer<AmiiboAppState, SetOrderCriteriaAction>(setOrderCriteriaReducer),
  TypedReducer<AmiiboAppState, SetAmiiboLoadingStateAction>(setAmiiboLoadingStateReducer),
]);

AmiiboAppState setAmiiboLoadingStateReducer(AmiiboAppState state, SetAmiiboLoadingStateAction action) {
  return state.copyWith(
    amiiboLoadingState: action.amiiboLoadingState,
  );
}

AmiiboAppState setOrderCriteriaReducer(AmiiboAppState state, SetOrderCriteriaAction action) {
  return state.copyWith(
    orderCriteria: action.orderCriteria,
  );
}

AmiiboAppState setAmiibosListReducer(AmiiboAppState state, SetAmiibosListAction action) {
  return state.copyWith(
    amiibosList: action.amiibosList,
  );
}

AmiiboAppState toggleInterfaceModeReducer(AmiiboAppState state, ToggleInterfaceMode action) {
  return state.copyWith(
    interfaceMode: state.interfaceMode == InterfaceMode.light ? InterfaceMode.dark : InterfaceMode.light,
  );
}

AmiiboAppState setInterfaceModeReducer(AmiiboAppState state, SetInterfaceModeAction action) {
  return state.copyWith(
    interfaceMode: action.interfaceMode,
  );
}
