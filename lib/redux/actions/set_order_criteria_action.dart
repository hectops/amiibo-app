
import 'package:amiibo/redux/state/amiibo_app_state.dart';
import 'package:flutter/foundation.dart';

class SetOrderCriteriaAction {
  final OrderCriteria orderCriteria;

  SetOrderCriteriaAction({@required this.orderCriteria});
}