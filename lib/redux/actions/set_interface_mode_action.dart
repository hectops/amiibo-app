
import 'package:amiibo/redux/state/amiibo_app_state.dart';
import 'package:flutter/foundation.dart';

class SetInterfaceModeAction {
  final InterfaceMode interfaceMode;

  SetInterfaceModeAction({@required this.interfaceMode});
}