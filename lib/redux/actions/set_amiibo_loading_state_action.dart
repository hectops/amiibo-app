
import 'package:amiibo/redux/state/amiibo_app_state.dart';
import 'package:flutter/foundation.dart';

class SetAmiiboLoadingStateAction {
  final LoadingState amiiboLoadingState;

  SetAmiiboLoadingStateAction({@required this.amiiboLoadingState});
}