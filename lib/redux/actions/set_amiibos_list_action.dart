
import 'package:amiibo/model/amiibo.dart';
import 'package:flutter/foundation.dart';

class SetAmiibosListAction {
  final List<Amiibo> amiibosList;

  SetAmiibosListAction({@required this.amiibosList});
}