import 'package:amiibo/redux/actions/get_shared_prefs_action.dart';
import 'package:amiibo/redux/actions/set_interface_mode_action.dart';
import 'package:amiibo/redux/actions/set_order_criteria_action.dart';
import 'package:amiibo/redux/actions/toggle_interface_mode_action.dart';
import 'package:amiibo/redux/state/amiibo_app_state.dart';
import 'package:redux/redux.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesMiddleware {

  static void setOrderCriteriaMiddleware(Store<AmiiboAppState> amiiboAppStore, SetOrderCriteriaAction action, NextDispatcher next) {
    next(action);
    _saveToPrefs(amiiboAppStore.state);
  }

  static void toggleInterfaceModeMiddleware(Store<AmiiboAppState> amiiboAppStore, ToggleInterfaceMode action, NextDispatcher next) {
    next(action);
    _saveToPrefs(amiiboAppStore.state);
  }

  static void getSharedPrefsMiddleWare(Store<AmiiboAppState> amiiboAppStore, GetSharedPrefsAction action, NextDispatcher next) async {
    next(action);
    await _loadFromPrefs().then(
      (amiiboAppState) {
        amiiboAppStore.dispatch(SetInterfaceModeAction(interfaceMode: amiiboAppState.interfaceMode));
        amiiboAppStore.dispatch(SetOrderCriteriaAction(orderCriteria: amiiboAppState.orderCriteria));
      }
    );
  }

  static void _saveToPrefs(AmiiboAppState amiiboAppState) async {
    print("Saving " + amiiboAppState.interfaceMode.toString() + " to preferences");
    print("Saving " + amiiboAppState.orderCriteria.toString() + " to preferences");
    SharedPreferences prefrences = await SharedPreferences.getInstance();
    String interfaceMode = amiiboAppState.interfaceMode.toString();
    String orderCriteria = amiiboAppState.orderCriteria.toString();
    await prefrences.setString("interfaceMode", interfaceMode);
    await prefrences.setString("orderCriteria", orderCriteria);
  }

  static Future<AmiiboAppState> _loadFromPrefs() async {
    print("Loading3 from preferences");
    SharedPreferences prefrences = await SharedPreferences.getInstance();
    String interfaceMode = prefrences.getString("interfaceMode");
    String orderCriteria = prefrences.getString("orderCriteria");

    InterfaceMode mode = interfaceMode == "InterfaceMode.light" ? InterfaceMode.light : InterfaceMode.dark;

    OrderCriteria criteria;

    switch (orderCriteria) {
      case "OrderCriteria.amiiboSeries":
        criteria = OrderCriteria.amiiboSeries;
        break;
      case "OrderCriteria.gameSeries":
        criteria = OrderCriteria.gameSeries;
        break;
      case "OrderCriteria.character":
        criteria = OrderCriteria.character;
        break;
      case "OrderCriteria.type":
        criteria = OrderCriteria.type;
        break;
      default:
        criteria = OrderCriteria.amiiboSeries;
    }

    return AmiiboAppState.initialState().copyWith(interfaceMode: mode, orderCriteria: criteria);
  }
}