import 'package:amiibo/redux/actions/get_amiibos_list_action.dart';
import 'package:amiibo/redux/actions/get_shared_prefs_action.dart';
import 'package:amiibo/redux/actions/set_order_criteria_action.dart';
import 'package:amiibo/redux/actions/toggle_interface_mode_action.dart';
import 'package:amiibo/redux/middleware/internet_calls_middleware.dart';
import 'package:amiibo/redux/middleware/shared_preferences_middleware.dart';
import 'package:amiibo/redux/state/amiibo_app_state.dart';
import 'package:redux/redux.dart';

List<Middleware<AmiiboAppState>> amiiboAppMiddleware() {
  return [
    TypedMiddleware<AmiiboAppState, GetAmiibosListAction>(InternetCallsMiddleware.getAmiibosListMiddleware),
    TypedMiddleware<AmiiboAppState, GetSharedPrefsAction>(SharedPreferencesMiddleware.getSharedPrefsMiddleWare),
    TypedMiddleware<AmiiboAppState, ToggleInterfaceMode>(SharedPreferencesMiddleware.toggleInterfaceModeMiddleware),
    TypedMiddleware<AmiiboAppState, SetOrderCriteriaAction>(SharedPreferencesMiddleware.setOrderCriteriaMiddleware),
  ];
}