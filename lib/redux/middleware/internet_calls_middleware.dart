import 'dart:convert';

import 'package:amiibo/model/amiibo.dart';
import 'package:amiibo/redux/actions/get_amiibos_list_action.dart';
import 'package:amiibo/redux/actions/set_amiibo_loading_state_action.dart';
import 'package:amiibo/redux/actions/set_amiibos_list_action.dart';
import 'package:amiibo/redux/amiibo_app_state_store.dart';
import 'package:amiibo/redux/state/amiibo_app_state.dart';
import 'package:http/http.dart' as http;
import 'package:redux/redux.dart';

class InternetCallsMiddleware {

  static void getAmiibosListMiddleware(Store<AmiiboAppState> amiiboAppStore, GetAmiibosListAction action, NextDispatcher next) {
    next(action);
    _getAmiibosList();
  }

  static void _getAmiibosList() async {
    http.get("http://www.amiiboapi.com/api/amiibo/")
      .then((response) {
        if(response.statusCode == 200) {
          List<Amiibo> amiiboList = [];
          json.decode(response.body)["amiibo"].forEach((amiiboJson) => amiiboList.add(Amiibo.fromJson(amiiboJson)));
          amiiboAppStore.dispatch(SetAmiibosListAction(amiibosList: amiiboList));
          amiiboAppStore.dispatch(SetAmiiboLoadingStateAction(amiiboLoadingState: LoadingState.didLoad));
          print(amiiboList);
        }
        else {
          amiiboAppStore.dispatch(SetAmiiboLoadingStateAction(amiiboLoadingState: LoadingState.failedToLoad));
          print("failed to load amiibos");
        }
      }).catchError((){
        amiiboAppStore.dispatch(SetAmiiboLoadingStateAction(amiiboLoadingState: LoadingState.failedToLoad));
        print("failed to load amiibos");
      }).timeout(Duration(seconds: 20), onTimeout: () {
        amiiboAppStore.dispatch(SetAmiiboLoadingStateAction(amiiboLoadingState: LoadingState.failedToLoad));
        print("failed to load amiibos");
      });
  }
}