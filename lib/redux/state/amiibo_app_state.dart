import 'package:amiibo/model/amiibo.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

enum InterfaceMode {light, dark,}
enum OrderCriteria {amiiboSeries, character, gameSeries, type,}
enum LoadingState {isLoading, didLoad, failedToLoad,}

class AmiiboAppState {
  final List<Amiibo> amiibosList;
  final OrderCriteria orderCriteria;
  final InterfaceMode interfaceMode;
  final LoadingState amiiboLoadingState;

  ThemeData get theme {
    switch (interfaceMode) {
      case InterfaceMode.light:
        return ThemeData(
          brightness: Brightness.light,
          primarySwatch: Colors.purple
        );
      case InterfaceMode.dark:
        return ThemeData(
          brightness: Brightness.dark,
          accentColor: Colors.redAccent
        );
      default:
        return ThemeData(
          brightness: Brightness.light,
          primarySwatch: Colors.purple
        );
    }
  }

  AmiiboAppState({
    @required this.amiibosList,
    @required this.interfaceMode,
    @required this.orderCriteria,
    @required this.amiiboLoadingState
  });

  AmiiboAppState.initialState() : 
      amiibosList = [],
      interfaceMode = InterfaceMode.light,
      orderCriteria = OrderCriteria.amiiboSeries,
      amiiboLoadingState = LoadingState.isLoading;

  AmiiboAppState copyWith({
      List<Amiibo> amiibosList,
      InterfaceMode interfaceMode,
      String filterString,
      OrderCriteria orderCriteria,
      LoadingState amiiboLoadingState}) {

    return AmiiboAppState(
      amiibosList: amiibosList ?? this.amiibosList,
      interfaceMode: interfaceMode ?? this.interfaceMode,
      orderCriteria: orderCriteria ?? this.orderCriteria,
      amiiboLoadingState: amiiboLoadingState ?? this.amiiboLoadingState,
    );
  }
}