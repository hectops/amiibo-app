

import 'package:amiibo/redux/state/amiibo_app_state.dart';
import 'package:amiibo/redux/reducers/amiibos_app_reducer.dart';
import 'package:redux/redux.dart';

import 'package:amiibo/redux/middleware/amiibo_app_middleware.dart';


final Store amiiboAppStore = Store<AmiiboAppState>(
  amiiboAppReducer, 
  initialState: AmiiboAppState.initialState(),
  middleware: amiiboAppMiddleware()
);
