import 'package:amiibo/redux/actions/get_shared_prefs_action.dart';
import 'package:amiibo/redux/amiibo_app_state_store.dart';
import 'package:amiibo/redux/state/amiibo_app_state.dart';
import 'package:amiibo/ui/screens/amiibo_about_screen.dart';
import 'package:amiibo/ui/screens/amiibo_detail_screen.dart';
import 'package:amiibo/ui/screens/amiibo_home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

void main() { 
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(AmiiboApp());
}

class AmiiboApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {



    return StoreProvider<AmiiboAppState>(
      store: amiiboAppStore,
      child: StoreConnector<AmiiboAppState, ThemeData>(
        onInit: (amiiboAppStore) => amiiboAppStore.dispatch(GetSharedPrefsAction()),
        converter: (Store<AmiiboAppState> amiiboAppStore) => amiiboAppStore.state.theme,
        builder: (BuildContext context, ThemeData theme) {
          return MaterialApp(
            theme: theme,
            routes: <String, WidgetBuilder>{
              '/':      (BuildContext context) => AmiiboHomeScreen(),
              '/about': (BuildContext context) => AmiiboAboutScreen(),
            },
            onGenerateRoute: _getRoute,
          );
        },
      )
    );
  }

  Route<dynamic> _getRoute(RouteSettings settings) {
    // Routes, by convention, are split on slashes, like filesystem paths.
    final List<String> path = settings.name.split('/');
    // We only support paths that start with a slash, so bail if
    // the first component is not empty:
    if (path[0] != '')
      return null;
    // If the path is "/amiibo:..." then show a amiibo page for the
    // specified amiibo.
    if (path[1].startsWith('amiibo:')) {
      // We don't yet support subpages of a amiibo, so bail if there's
      // any more path components.
      if (path.length != 2)
        return null;
      // Extract the index part of "amiibo:..." and return a route
      // for that amiibo.
      final int index = int.parse(path[1].substring(7));
      return MaterialPageRoute<void>(
        settings: settings,
        builder: (BuildContext context) => AmiiboDetailScreen(amiibo: amiiboAppStore.state.amiibosList[index],),
      );
    }
    // The other paths we support are in the routes table.
    return null;
  }
}